<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/setClient', 'App\Http\Controllers\ClientController@setClient');
Route::get('/countHosts', 'App\Http\Controllers\InfoController@countHosts');
Route::get('/getFileSize', 'App\Http\Controllers\InfoController@getFileSize');
