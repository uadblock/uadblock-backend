<?php
namespace App\Models;

class Clients extends \App\Models\AbstractModels\AbstractClients
{
    protected $fillable = ['pushtoken', 'allownotification', 'version', 'language'];
}
