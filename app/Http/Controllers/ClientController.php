<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function setClient (Request $request){

        $request->validate([
            'pushtoken' => 'required',
            'allownotification' => 'required',
            'version' => 'required'
        ]);

        $allow = ($request->allownotification == 'true')? true : false;

        $client = Clients::updateOrCreate(
            ['pushtoken' => $request->pushtoken],
            ['allownotification' => $allow, 'version' => $request->version, 'language' => $request->language]
        );

        return response()->json(null, 200);
    }

}
