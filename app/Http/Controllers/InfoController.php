<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class InfoController extends Controller
{
    public function countHosts(Request $request)
    {

        $filesystem = new Filesystem();
        $path = '/srv/files.uadblock.org/';

        if ($request->hostfile != '' && $filesystem->exists($path . $request->hostfile)) {
            $process = new Process(['wc', '-l', $path . $request->hostfile]);
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            $lines = intval(explode(" ", $process->getOutput())[0]) - 11;
            return response()->json($lines);
        }
    }

    public function getFileSize(Request $request)
    {

        $filesystem = new Filesystem();
        $path = '/srv/files.uadblock.org/';

        if ($request->hostfile != '' && $filesystem->exists($path . $request->hostfile)) {
            $process = new Process(['du', '-B1', $path . $request->hostfile]);
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $size = $this->formatSizeUnits(intval(explode(" ", $process->getOutput())[0]));
            return response()->json($size);
        }
    }

    private function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
